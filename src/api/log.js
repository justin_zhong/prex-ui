import { axios } from '@/utils/request'

export function queryLogList(parms) {
  return axios({
    url: '/sys/log',
    method: 'get',
    params: parms
  })
}

export function deleteLog(id) {
  return axios({
    url: '/sys/log/' + id,
    method: 'delete'
  })
}
