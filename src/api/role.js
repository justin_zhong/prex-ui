import { axios } from '@/utils/request'

export function queryRoleList(parms) {
  return axios({
    url: '/sys/role',
    method: 'get',
    params: parms
  })
}

// 根据主键删除角色
export function deleteRole(id) {
  return axios({
    url: '/sys/role/' + id,
    method: 'delete'
  })
}

export function batchDeleteRole(parameter) {
  return axios({
    url: '/sys/role/batchDelete',
    method: 'delete',
    params: parameter
  })
}

// 添加角色
export function addRole(data) {
  return axios({
    url: '/sys/role',
    method: 'post',
    data: data
  })
}

/**
 * 更新角色
 * @param data
 */
export function updateRole(data) {
  return axios({
    url: '/sys/role',
    method: 'put',
    data: data
  })
}
/**
 * 更新角色权限
 * @param data
 */
export function updateRolePermission(data) {
  return axios({
    url: '/sys/role/updateRolePermission',
    method: 'put',
    data: data
  })
}

/**
 * 根据角色id查找菜单
 * @param id
 */
export function queryRolePermission(id) {
  return axios({
    url: '/sys/role/queryRolePermission/' + id,
    method: 'get'
  })
}
