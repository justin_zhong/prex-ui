// eslint-disable-next-line
import { UserLayout, BasicLayout, RouteView, BlankLayout, PageView } from '@/layouts'
import { bxAnaalyse } from '@/core/icons'

export const asyncRouterMap = [

  {
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: { title: '首页' },
    redirect: '/dashboard/workplace',
    children: [
      // dashboard
      {
        path: '/dashboard',
        name: 'dashboard',
        redirect: '/dashboard/analysis',
        component: RouteView,
        meta: { title: '主页', keepAlive: true, icon: bxAnaalyse},
        children: [
          {
            path: '/dashboard/analysis',
            name: 'Analysis',
            component: () => import('@/views/dashboard/Analysis'),
            meta: { title: '分析页', keepAlive: false}
          },
          // 外部链接
          {
            path: 'https://www.baidu.com/',
            name: 'Monitor',
            meta: { title: '监控页（外部）', target: '_blank' }
          }
        ]
      },

      // admin

      {
        path: '/admin',
        redirect: '/admin/user',
        component: PageView,
        meta: { title: '权限管理', icon: 'form'},
        children: [
          {
            path: 'user',
            name: 'user',
            component: () => import('@/views/admin/User'),
            meta: { title: '用户管理',icon: 'user', keepAlive: true}
          },
          {
            path: 'role',
            name: 'role',
            component: () => import('@/views/admin/Role'),
            meta: { title: '角色管理',icon:'team', keepAlive: true}
          },
          {
            path: 'menu',
            name: 'menu',
            component: () => import('@/views/admin/Menu'),
            meta: { title: '菜单管理',icon:'bars', keepAlive: true}
          },
          {
            path: 'dept',
            name: 'dept',
            component: () => import('@/views/admin/Dept'),
            meta: { title: '部门管理',icon:'cluster', keepAlive: true}
          },
          {
            path: '/admin/job',
            name: 'job',
            component: () => import('@/views/admin/Job.vue'),
            meta: { title: '岗位管理',icon:'user-add', keepAlive: true}
          },
        ]
      },
      {
        path: '/system',
        redirect: '/system/dict',
        component: RouteView,
        meta: { title: '系统管理', icon: 'setting'},
        children: [
          {
            path: '/system/dict',
            name: 'dict',
            component: () => import('@/views/system/Dict'),
            meta: { title: '数据字典',icon: 'sliders', keepAlive: true}
          },
          {
            path: '/system/log',
            name: 'log',
            component: () => import('@/views/system/Log'),
            meta: { title: '日志管理',icon: 'exception', keepAlive: true}
          },
          {
            path: '/system/client',
            name: 'client',
            component: () => import('@/views/system/Client'),
            meta: { title: '客户端管理',icon: 'exception', keepAlive: true}
          },

        ]
      },
      {
        path: '/monitor',
        redirect: '/monitor/redis',
        component: PageView,
        meta: { title: '系统监控', icon: 'dashboard'},
        children: [
          {
            path: '/monitor/redis',
            name: 'redis',
            component: () => import('@/views/monitor/RedisInfo'),
            meta: { title: '缓存监控',icon: 'user', keepAlive: true}
          },
          {
            path: '/monitor/disk',
            name: 'disk',
            component: () => import('@/views/monitor/DiskMonitoring'),
            meta: { title: '磁盘监控',icon: 'user', keepAlive: true}
          }

        ]
      },

      // profile
      // {
      //   path: '/profile',
      //   name: 'profile',
      //   component: RouteView,
      //   redirect: '/profile/basic',
      //   meta: { title: '详情页', icon: 'profile', permission: [ 'profile' ] },
      //   children: [
      //     {
      //       path: '/profile/basic',
      //       name: 'ProfileBasic',
      //       component: () => import('@/views/profile/basic/Index'),
      //       meta: { title: '基础详情页', permission: [ 'profile' ] }
      //     },
      //     {
      //       path: '/profile/advanced',
      //       name: 'ProfileAdvanced',
      //       component: () => import('@/views/profile/advanced/Advanced'),
      //       meta: { title: '高级详情页', permission: [ 'profile' ] }
      //     }
      //   ]
      // },

      // Exception
      // {
      //   path: '/exception',
      //   name: 'exception',
      //   component: RouteView,
      //   redirect: '/exception/403',
      //   meta: { title: '异常页', icon: 'warning', permission: [ 'exception' ] },
      //   children: [
      //     {
      //       path: '/exception/403',
      //       name: 'Exception403',
      //       component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/403'),
      //       meta: { title: '403', permission: [ 'exception' ] }
      //     },
      //     {
      //       path: '/exception/404',
      //       name: 'Exception404',
      //       component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404'),
      //       meta: { title: '404', permission: [ 'exception' ] }
      //     },
      //     {
      //       path: '/exception/500',
      //       name: 'Exception500',
      //       component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/500'),
      //       meta: { title: '500', permission: [ 'exception' ] }
      //     }
      //   ]
      // },

      // account
      // {
      //   path: '/account',
      //   component: RouteView,
      //   redirect: '/account/center',
      //   name: 'account',
      //   hidden: true,
      //   meta: { title: '个人页', icon: 'user', keepAlive: true, permission: [ 'user' ] },
      //   children: [
      //     {
      //       path: '/account/center',
      //       name: 'settings',
      //       component: () => import('@/views/account/settings/Index'),
      //       meta: { title: '个人设置', hideHeader: true },
      //       redirect: '/account/settings/base',
      //       hideChildrenInMenu: true,
      //       children: [
      //         {
      //           path: '/account/settings/base',
      //           name: 'BaseSettings',
      //           component: () => import('@/views/account/settings/BaseSetting'),
      //           meta: { title: '基本设置', hidden: true }
      //         },
      //         {
      //           path: '/account/settings/security',
      //           name: 'SecuritySettings',
      //           component: () => import('@/views/account/settings/Security'),
      //           meta: { title: '安全设置', hidden: true, keepAlive: true }
      //         },
      //         {
      //           path: '/account/settings/custom',
      //           name: 'CustomSettings',
      //           component: () => import('@/views/account/settings/Custom'),
      //           meta: { title: '个性化设置', hidden: true, keepAlive: true }
      //         },
      //         {
      //           path: '/account/settings/binding',
      //           name: 'BindingSettings',
      //           component: () => import('@/views/account/settings/Binding'),
      //           meta: { title: '账户绑定', hidden: true, keepAlive: true }
      //         },
      //         {
      //           path: '/account/settings/notification',
      //           name: 'NotificationSettings',
      //           component: () => import('@/views/account/settings/Notification'),
      //           meta: { title: '新消息通知', hidden: true, keepAlive: true }
      //         }
      //       ]
      //     }
      //   ]
      // },
    ]
  },
  {
    path: '*', redirect: '/404', hidden: true
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/user',
    component: UserLayout,
    redirect: '/user/login',
    hidden: true,
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/Login')
      },
      {
        path: 'register',
        name: 'register',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/Register')
      },
      {
        path: 'register-result',
        name: 'registerResult',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/RegisterResult')
      }
    ]
  },

  {
    path: '/404',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404')
  }

]
